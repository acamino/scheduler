require 'scheduler/version'
require 'scheduler/talk'
require 'scheduler/support/time_formattable'
require 'scheduler/support/time_math'
require 'scheduler/scheduled_talk'
require 'scheduler/other_scheduled_event'
require 'scheduler/session'
require 'scheduler/scheduling_engine'
require 'scheduler/parser/title_validator'
require 'scheduler/parser/talk_parser'
require 'scheduler/parser/lightning_talk_parser'
require 'scheduler/parser'
require 'scheduler/conference'
require 'scheduler/conference/builder'
require 'scheduler/conference/track'
require 'scheduler/conference/printer'

module Scheduler
  module_function

  # Schedule talks for a conference.
  # @param path<String> the path to the talks file
  # @return <Scheduler::Conference>
  def schedule(path)
    entries = File.read(path).split("\n")
    talks = Scheduler::Parser.new(entries).parse
    Scheduler::SchedulingEngine.new(talks).schedule
  end

  # Schedule and print talks for a conference.
  # @param path<String> the path to the talks file
  def schedule_and_print(path, randomize = false, io = $stdout)
    conference = schedule(path)
    printer = Scheduler::Conference::Printer.new(
      conference, DateTime.parse('09:00'), DateTime.parse('13:00'), randomize
    )
    printer.print(io)
  end
end
