module Scheduler
  class ScheduledTalk
    attr_reader :title, :duration, :track

    def initialize(talk, track, session)
      @title = talk.title
      @duration = talk.duration
      @track = track
      @session_code = session.code
    end

    def morning?
      session_code == :morning
    end

    def afternoon?
      session_code == :afternoon
    end

    def to_s
      "#{title} #{duration} min"
    end

    attr_reader :session_code
  end
end
