module Scheduler
  class SchedulingEngine
    TOTAL_TRACK_MINUTES = 420

    def initialize(talks)
      @talks = talks
    end

    # rubocop:disable Metrics/MethodLength
    def schedule
      scheduled_talks = []

      (1..total_tracks).each do |track|
        morning_session   = Session.create_for_morning
        afternoon_session = Session.create_for_afternoon

        talks.each do |talk|
          if can_schedule?(talk, morning_session)
            schedule_talk!(scheduled_talks, talk, morning_session, track)
          end

          if can_schedule?(talk, afternoon_session)
            schedule_talk!(scheduled_talks, talk, afternoon_session, track)
          end
        end
      end

      Conference::Builder.new(scheduled_talks).build
    end

    private

    attr_reader :talks

    def schedule_talk!(scheduled_talks, talk, session, track)
      scheduled_talks << ScheduledTalk.new(talk, track, session)
      talk.schedule!
      session.consume_time_for(talk)
    end

    def can_schedule?(talk, session)
      talk.unscheduled? && session.remaining_minutes >= talk.duration
    end

    def total_tracks
      (@talks.map(&:duration).inject(:+) / TOTAL_TRACK_MINUTES.to_f).ceil
    end
  end
end
