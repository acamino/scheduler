module Scheduler
  module Support
    module TimeFormattable
      def format_time(time)
        time.strftime('%I:%M %p')
      end
    end
  end
end
