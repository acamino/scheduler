module Scheduler
  module Support
    module TimeMath
      def self.add_minutes(time, minutes)
        time + Rational(minutes, 1440)
      end
    end
  end
end
