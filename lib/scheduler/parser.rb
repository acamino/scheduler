module Scheduler
  class Parser
    def initialize(entries)
      @entries = entries
    end

    def parse
      @entries.map do |entry|
        TalkParser.parse(entry) || LightningTalkParser.parse(entry)
      end
    end
  end
end
