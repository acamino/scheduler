module Scheduler
  Talk = Struct.new(:title, :duration, :scheduled) do
    def unscheduled?
      !scheduled
    end

    def schedule!
      self.scheduled = true
    end
  end
end
