module Scheduler
  class OtherScheduledEvent
    include Support::TimeFormattable

    attr_reader :start_time, :title

    def self.create_lunch
      new('Lunch', DateTime.parse('12:00'))
    end

    def self.create_networking_event
      new('Networking Event', DateTime.parse('17:00'))
    end

    def initialize(title, start_time)
      @title = title
      @start_time = start_time
    end

    def to_s
      "#{format_time(start_time)} #{title}"
    end
  end
end
