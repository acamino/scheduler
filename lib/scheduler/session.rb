require 'date'

module Scheduler
  class Session
    attr_accessor :code, :remaining_minutes

    MORNING_SESSION_MINUTES = 180
    AFTERNOON_SESSION_MINUTES = 240

    def self.create_for_morning
      new(:morning, MORNING_SESSION_MINUTES)
    end

    def self.create_for_afternoon
      new(:afternoon, AFTERNOON_SESSION_MINUTES)
    end

    def initialize(code, remaining_minutes)
      @code = code
      @remaining_minutes = remaining_minutes
    end

    def consume_time_for(talk)
      @remaining_minutes -= talk.duration
    end
  end
end
