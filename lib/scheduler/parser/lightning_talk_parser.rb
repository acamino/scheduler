module Scheduler
  class Parser
    class LightningTalkParser
      TOKEN = /\slightning$/
      LIGHTNING_TALK_DURATION = 5

      def self.parse(entry)
        new(entry).parse
      end

      def parse
        return unless parsable?

        raise InvalidTitleError, "invalid title: #{title}" unless TitleValidator.valid?(title)
        Talk.new(title, LIGHTNING_TALK_DURATION, false)
      end

      private

      attr_reader :entry

      def initialize(entry)
        @entry = entry
      end

      def title
        @title ||= entry.split(TOKEN).first
      end

      def parsable?
        entry.match(TOKEN)
      end
    end
  end
end
