module Scheduler
  class Parser
    class TalkParser
      TOKEN = /\s(\d+)min/

      def self.parse(entry)
        new(entry).parse
      end

      def parse
        title, duration = title_and_duration
        raise InvalidTitleError, "invalid title: #{title}" unless TitleValidator.valid?(title)
        Talk.new(title, duration.to_i, false) if duration
      end

      private

      def initialize(entry)
        @entry = entry
      end

      def title_and_duration
        @entry.split(TOKEN)
      end
    end
  end
end
