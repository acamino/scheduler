module Scheduler
  class Parser
    module TitleValidator
      def self.valid?(title)
        !title.match(/\d/)
      end
    end

    class InvalidTitleError < ArgumentError; end
  end
end
