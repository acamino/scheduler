module Scheduler
  class Conference
    class Builder
      def initialize(scheduled_talks)
        @scheduled_talks = scheduled_talks
      end

      def build
        tracks = track_ids.map do |track_id|
          Track.new(morning_talks(track_id), afternoon_talks(track_id))
        end
        Conference.new(tracks)
      end

      private

      attr_reader :scheduled_talks

      def track_ids
        scheduled_talks.map(&:track).uniq
      end

      def morning_talks(track)
        scheduled_talks.select { |t| t.track == track && t.morning? }
      end

      def afternoon_talks(track)
        scheduled_talks.select { |t| t.track == track && t.afternoon? }
      end
    end
  end
end
