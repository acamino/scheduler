module Scheduler
  class Conference
    class Track
      def initialize(morning_talks, afternoon_talks)
        @morning_talks = morning_talks
        @afternoon_talks = afternoon_talks
      end

      def events
        [
          @morning_talks,
          OtherScheduledEvent.create_lunch,
          @afternoon_talks,
          OtherScheduledEvent.create_networking_event
        ]
      end
    end
  end
end
