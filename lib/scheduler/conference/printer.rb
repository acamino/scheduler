module Scheduler
  class Conference
    class Printer
      include Support::TimeFormattable

      def initialize(conference, morning_start_time, afternoon_start_time, randomize)
        @conference = conference
        @morning_start_time = morning_start_time
        @afternoon_start_time = afternoon_start_time
        @randomize = randomize
      end

      def print(io = $stdout)
        conference.tracks.each do |track|
          morning_talks, lunch, afternoon_talks, special_event = track.events

          io.puts printable_talks(morning_talks, morning_start_time)
          io.puts lunch
          io.puts printable_talks(afternoon_talks, afternoon_start_time)
          io.puts special_event
          io.puts
        end
      end

      private

      attr_reader :conference, :morning_start_time, :afternoon_start_time, :randomize

      alias randomize? randomize

      def printable_talks(talks, talk_start_time)
        randomize(talks).map do |talk|
          printable_talk = "#{format_time(talk_start_time)} #{talk}"
          talk_start_time = Support::TimeMath.add_minutes(talk_start_time, talk.duration)
          printable_talk
        end
      end

      def randomize(talks)
        if randomize?
          talks.shuffle
        else
          talks
        end
      end
    end
  end
end
