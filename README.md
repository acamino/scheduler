# Conference Tracker

This library is my solution for the "Conference Track Management" challenge.

## Local Development

1. Fork the project [on Bitbucket](https://bitbucket.org/acamino/scheduler)
   and clone your fork locally.

        $ git clone git@bitbucket.org:username/scheduler.git
        $ cd scheduler
        $ git remote add upstream https://acamino@bitbucket.org/acamino/scheduler.git

1. Run the setup script.

        $ bin/setup

1. Make sure the tests succeed.

        $ rake test

## Quick start

If you want to play with this library you can run:

        $ bin/console

        $ Scheduler.schedule_and_print('/path/to/talks')
        09:00 AM Writing Fast Tests Against Enterprise Rails 60 min
        10:00 AM Overdoing it in Python 45 min
        10:45 AM Lua for the Masses 30 min
        11:15 AM Ruby Errors from Mismatched Gem Versions 45 min
        12:00 PM Lunch
        ...

Another possibility is to use the `scheduler` executable:

        $ bin/scheduler --filepath /path/to/talks
        09:00 AM Writing Fast Tests Against Enterprise Rails 60 min
        10:00 AM Overdoing it in Python 45 min
        10:45 AM Lua for the Masses 30 min
        11:15 AM Ruby Errors from Mismatched Gem Versions 45 min
        12:00 PM Lunch
        ...

## Contributing

Edits and enhancements are welcome. Just fork the repository, make your changes
and send me a pull request.

## Licence

The code in this repository is licensed under the terms of the
[MIT License](http://www.opensource.org/licenses/mit-license.html).  
Please see the [LICENSE](LICENSE) file for details.
