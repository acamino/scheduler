require 'test_helper'

describe Scheduler do
  it 'has a version number' do
    ::Scheduler::VERSION.wont_be_nil
  end

  describe '.schedule_and_print' do
    it 'schedules and prints a given set of talks' do
      talks_path = 'test/fixtures/talks'

      io = StringIO.new
      Scheduler.schedule_and_print(talks_path, false, io)

      expected_output = [
        '09:00 AM Writing Fast Tests Against Enterprise Rails 60 min',
        '10:00 AM Overdoing it in Python 45 min',
        '10:45 AM Lua for the Masses 30 min',
        '11:15 AM Ruby Errors from Mismatched Gem Versions 45 min',
        '12:00 PM Lunch',
        '01:00 PM Common Ruby Errors 45 min',
        '01:45 PM Rails for Python Developers 5 min',
        '01:50 PM Communicating Over Distance 60 min',
        '02:50 PM Accounting-Driven Development 45 min',
        '03:35 PM Woah 30 min',
        '04:05 PM Sit Down and Write 30 min',
        '05:00 PM Networking Event',
        '',
        '09:00 AM Pair Programming vs Noise 45 min',
        '09:45 AM Rails Magic 60 min',
        '10:45 AM Ruby on Rails: Why We Should Move On 60 min',
        '12:00 PM Lunch',
        '01:00 PM Clojure Ate Scala (on my project) 45 min',
        '01:45 PM Programming in the Boondocks of Seattle 30 min',
        '02:15 PM Ruby vs. Clojure for Back-End Development 30 min',
        '02:45 PM Ruby on Rails Legacy App Maintenance 60 min',
        '03:45 PM A World Without HackerNews 30 min',
        '04:15 PM User Interface CSS in Rails Apps 30 min',
        '05:00 PM Networking Event',
        '',
        ''
      ].join("\n")

      io.rewind
      io.read.must_equal(expected_output)
    end
  end
end
