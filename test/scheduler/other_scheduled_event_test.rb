require 'date'
require_relative '../test_helper'

describe Scheduler::ScheduledTalk do
  describe '#to_s' do
    it 'returns other scheduled event as string' do
      other_scheduled_event = Scheduler::OtherScheduledEvent.new(
        'Lunch', DateTime.parse('12:00')
      )

      other_scheduled_event.to_s.must_equal('12:00 PM Lunch')
    end
  end
end
