require_relative '../test_helper'

describe Scheduler::Parser do
  describe '#parser' do
    it 'parsers a list of entries' do
      entries = [
        'Rails for Python Developers lightning',
        'Writing Fast Tests Against Enterprise Rails 60min'
      ]

      talks = Scheduler::Parser.new(entries).parse
      expected_talks = [
        Scheduler::Talk.new('Rails for Python Developers', 5, false),
        Scheduler::Talk.new('Writing Fast Tests Against Enterprise Rails', 60, false)
      ]
      talks.must_equal(expected_talks)
    end
  end
end
