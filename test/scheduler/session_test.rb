require_relative '../test_helper'

describe Scheduler::Session do
  describe '#consume_time_for' do
    it 'consumes the remaining minutes of a session for a talk' do
      morning_session = Scheduler::Session.new(:morning, 180)
      talk = Scheduler::Talk.new('title', 60, _)
      morning_session.consume_time_for(talk)
      morning_session.remaining_minutes.must_equal(120)
    end
  end
end
