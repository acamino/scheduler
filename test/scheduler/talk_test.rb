require_relative '../test_helper'

describe Scheduler::Talk do
  describe '#unscheduled?' do
    context 'when the talk is unscheduled' do
      it 'returns true' do
        talk = Scheduler::Talk.new('title', 10, false)
        talk.unscheduled?.must_equal(true)
      end
    end

    context 'when the talk is scheduled' do
      it 'returns false' do
        talk = Scheduler::Talk.new('title', 10, true)
        talk.unscheduled?.must_equal(false)
      end
    end
  end

  describe 'schedule!' do
    it 'sets the scheduled property to true' do
      talk = Scheduler::Talk.new('title', 10, false)
      talk.schedule!
      talk.scheduled.must_equal(true)
    end
  end
end
