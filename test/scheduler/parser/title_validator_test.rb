require_relative '../../test_helper'

describe Scheduler::Parser::TitleValidator do
  context 'when title is valid' do
    it 'returns true' do
      title = 'Writing Fast Tests Against Enterprise Rails'
      Scheduler::Parser::TitleValidator.valid?(title).must_equal(true)
    end
  end

  context 'when title is invalid' do
    it 'returns false' do
      title = 'Writing 9 Fast Tests Against Enterprise Rails'
      Scheduler::Parser::TitleValidator.valid?(title).must_equal(false)
    end
  end
end
