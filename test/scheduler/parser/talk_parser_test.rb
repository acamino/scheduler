require_relative '../../test_helper'

describe Scheduler::Parser::TalkParser do
  describe '.parse' do
    context 'when entry is parseable' do
      context 'and the title is valid' do
        it 'parses a lightning talk' do
          entry = 'Writing Fast Tests Against Enterprise Rails 60min'
          talk = Scheduler::Parser::TalkParser.parse(entry)
          expected_talk = Scheduler::Talk.new(
            'Writing Fast Tests Against Enterprise Rails', 60, false
          )
          talk.must_equal(expected_talk)
        end
      end

      context 'and the title is invalid' do
        it 'raises an exception' do
          entry = 'Writing 9 Fast Tests Against Enterprise Rails 60min'
          lambda do
            Scheduler::Parser::TalkParser.parse(entry)
          end.must_raise(Scheduler::Parser::InvalidTitleError)
        end
      end
    end

    context 'when entry is not parseable' do
      it 'returns nil' do
        entry = 'Writing Fast Tests Against Enterprise Rails'
        talk = Scheduler::Parser::TalkParser.parse(entry)
        talk.must_be_nil
      end
    end
  end
end
