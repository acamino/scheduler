require_relative '../../test_helper'

describe Scheduler::Parser::LightningTalkParser do
  describe '.parse' do
    context 'when entry is parseable' do
      context 'and the title is valid' do
        it 'parses a lightning talk' do
          entry = 'Rails for Python Developers lightning'
          lightning_talk = Scheduler::Parser::LightningTalkParser.parse(entry)
          expected_lightning_talk = Scheduler::Talk.new(
            'Rails for Python Developers', 5, false
          )
          lightning_talk.must_equal(expected_lightning_talk)
        end
      end

      context 'and the title is invalid' do
        it 'raises an exception' do
          entry = '1 Rails for Python Developers lightning'
          lambda do
            Scheduler::Parser::LightningTalkParser.parse(entry)
          end.must_raise(Scheduler::Parser::InvalidTitleError)
        end
      end
    end

    context 'when entry is not parseable' do
      it 'returns nil' do
        entry = 'Writing Fast Tests Against Enterprise Rails'
        talk = Scheduler::Parser::LightningTalkParser.parse(entry)
        talk.must_be_nil
      end
    end
  end
end
