require 'date'
require_relative '../test_helper'

describe Scheduler::SchedulingEngine do
  before do
    talks = [
      Scheduler::Talk.new('writing', 60, false),
      Scheduler::Talk.new('overdoing', 45, false),
      Scheduler::Talk.new('lua', 30, false),
      Scheduler::Talk.new('ruby', 45, false),
      Scheduler::Talk.new('common', 45, false),
      Scheduler::Talk.new('rails', 5, false),
      Scheduler::Talk.new('communicating', 60, false),
      Scheduler::Talk.new('accounting', 45, false),
      Scheduler::Talk.new('woah', 30, false),
      Scheduler::Talk.new('sit', 30, false)
    ]
    conference = Scheduler::SchedulingEngine.new(talks).schedule
    events = conference.tracks.first.events
    @morning_talks, @lunch_event, @afternoon_talks, @networking_event = events
  end

  describe '#tracks' do
    context 'for morning talks' do
      it 'does not exceed 180 minutes' do
        @morning_talks.map(&:duration).inject(:+).must_be(:<=, 180)
      end
    end

    context 'for afternoon talks' do
      it 'does not exceed 240 minutes' do
        @afternoon_talks.map(&:duration).inject(:+).must_be(:<=, 240)
      end
    end

    context 'for lunch event' do
      it 'starts at noon' do
        @lunch_event.to_s.must_include('12:00 PM')
      end
    end

    context 'for networking event' do
      it 'starts at 5 PM' do
        @networking_event.to_s.must_include('05:00 PM')
      end
    end
  end
end
