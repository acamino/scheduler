require 'date'
require_relative '../test_helper'

describe Scheduler::ScheduledTalk do
  before do
    talk = Scheduler::Talk.new('title', 10, true)
    @morning_scheduled_talk = Scheduler::ScheduledTalk.new(
      talk, 1, Scheduler::Session.create_for_morning
    )
    @afternoon_scheduled_talk = Scheduler::ScheduledTalk.new(
      talk, 1, Scheduler::Session.create_for_afternoon
    )
  end

  describe '#morning?' do
    context 'when the talk belongs to a morning session' do
      it 'returns true' do
        @morning_scheduled_talk.morning?.must_equal(true)
      end
    end

    context 'when the talk belongs to an afternoon session' do
      it 'returns false' do
        @afternoon_scheduled_talk.morning?.must_equal(false)
      end
    end
  end

  describe '#afternoon?' do
    context 'when the talk belongs to an afternoon session' do
      it 'returns true' do
        @afternoon_scheduled_talk.afternoon?.must_equal(true)
      end
    end

    context 'when the talk belongs to a morning session' do
      it 'returns false' do
        @morning_scheduled_talk.afternoon?.must_equal(false)
      end
    end
  end

  describe '#to_s' do
    it 'returns the scheduled talk as string' do
      @morning_scheduled_talk.to_s.must_equal(
        'title 10 min'
      )
    end
  end
end
